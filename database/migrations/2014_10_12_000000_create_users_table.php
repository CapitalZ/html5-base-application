<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->integer('family_id')->nullable();
            $table->integer('testament_user_id')->nullable();
            $table->integer('health')->default(0);
            $table->integer('cash')->default(0);
            $table->integer('bank')->default(0);
            $table->integer('rank')->default(0);
            $table->integer('exp')->default(0);
            $table->integer('bullets')->default(0);
            $table->integer('power')->default(0);
            $table->string('active_country')->default('nl_NL')->nullable();
            $table->integer('kudos')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
