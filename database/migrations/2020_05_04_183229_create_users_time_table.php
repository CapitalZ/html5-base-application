<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_time', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('timer_1')->default(0);
            $table->integer('timer_2')->default(0);
            $table->integer('timer_3')->default(0);
            $table->integer('timer_4')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_time');
    }
}
