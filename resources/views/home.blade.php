@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="d-block mb-4">
                    <a href="#" class="global-main-btn">
                        <i class="fas fa-shield-alt"></i> Equip character
                    </a>
                </div>
                <div class="d-block mb-4">
                    <a href="#" class="global-main-btn">
                        <i class="fas fa-shield-alt"></i> Start mission
                    </a>
                </div>
                <div class="d-block mb-4">
                    <a href="#" class="global-main-btn">
                        <i class="fas fa-shield-alt"></i> Explore map
                    </a>
                </div>
                <div class="d-block">
                    <a href="#" class="global-main-btn">
                        <i class="fas fa-shield-alt"></i> Shop
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                <code>
    <pre style="color: white;">
    @php var_dump(Auth::user()); @endphp
    </pre>
                </code>
            </div>
        </div>
    </div>
@endsection
