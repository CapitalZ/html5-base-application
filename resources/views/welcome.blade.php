<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    {{--        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">--}}
    <script src="https://kit.fontawesome.com/187c0ed624.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/anims.css') }}">
    <link rel="stylesheet" href="{{ asset('css/web.css') }}">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-default fixed-top">
    <a class="navbar-brand" href="#">{{ config('app.name', 'Laravel') }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            {{--                <li class="nav-item dropdown">--}}
            {{--                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
            {{--                        Dropdown--}}
            {{--                    </a>--}}
            {{--                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
            {{--                        <a class="dropdown-item" href="#">Action</a>--}}
            {{--                        <a class="dropdown-item" href="#">Another action</a>--}}
            {{--                        <div class="dropdown-divider"></div>--}}
            {{--                        <a class="dropdown-item" href="#">Something else here</a>--}}
            {{--                    </div>--}}
            {{--                </li>--}}
            {{--                <li class="nav-item">--}}
            {{--                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>--}}
            {{--                </li>--}}
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <div class="btn-group web-sidenav" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-primary"><i class="fas fa-chevron-down"></i></button>
                <button type="button" class="btn btn-primary"><i class="far fa-bell"></i></button>
                <div class="btn-group dropleft" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="far fa-user-circle"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        @if (Route::has('login'))
                            @auth
                                <span class="dropdown-item"> {{ Auth::user()->name }}</span>
                                <a class="dropdown-item" href="{{ route('home') }}"><i class="fas fa-sign-in-alt"></i> Continue</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @else
                                <form class="px-4 py-3" method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="email@example.com" required autocomplete="email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-12 mt-3">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" required autocomplete="current-password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-sm-6 mt-3">
                                            <button type="submit" class="btn btn-primary">Sign in</button>
                                        </div>
                                        <div class="form-check col-12 col-sm-6 mt-3">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="remember">Remember me</label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('register') }}">New around here? Sign up</a>
                                @if (Route::has('password.request'))
                                    <a class="dropdown-item" href="{{ route('password.request') }}">Forgot password?</a>
                                @endif
                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="carousel-cover" style="background-image: url({{ asset('/img/1547-1.jpg') }})"></div>
            {{--                <img src="{{ asset('/img/1547-1.jpg') }}" class="d-block w-100" alt="...">--}}
            <div class="carousel-caption d-none d-md-block">
                <h5>First slide label</h5>
                <p>Cupcake ipsum dolor sit. Amet lemon drops cookie tootsie roll chupa chups toffee chocolate cake oat cake gummi bears. Fruitcake cake chocolate cake danish muffin. Candy chocolate cheesecake pastry. Gummies chocolate bear claw gummi bears chupa chups pastry chocolate. Soufflé oat cake pastry gummi bears.</div>
        </div>
        <div class="carousel-item">
            <div class="carousel-cover" style="background-image: url({{ asset('/img/1547-2.jpg') }})"></div>
            {{--                                <img src="{{ asset('/img/1547-2.jpg') }}" class="d-block w-100" alt="...">--}}
            <div class="carousel-caption d-none d-md-block">
                <h5>Second slide label</h5>
                <p>Cheesecake tart dragée tart tootsie roll. Marzipan pie icing brownie chocolate bar sesame snaps muffin jelly-o. Sweet brownie I love lemon drops. Jujubes chocolate chocolate bar marzipan fruitcake marzipan candy. Bonbon cupcake croissant topping chupa chups. Lemon drops ice cream soufflé gummies jelly dragée jelly beans cheesecake. Marzipan oat cake danish I love muffin wafer cookie.</p>
            </div>
        </div>
        <div class="carousel-item">
            <div class="carousel-cover" style="background-image: url({{ asset('/img/1547-3.jpg') }})"></div>
            {{--                                <img src="{{ asset('/img/1547-3.jpg') }}" class="d-block w-100" alt="...">--}}
            <div class="carousel-caption d-none d-md-block">
                <h5>Third slide label</h5>
                <p>Icing tiramisu tiramisu topping biscuit I love I love chocolate cake. Cupcake tootsie roll tiramisu chocolate bar. I love tootsie roll candy canes jelly-o. Muffin sweet roll I love gummies cake. Danish cotton candy cheesecake cake marshmallow gummi bears I love gingerbread pastry. Wafer pudding brownie pie candy canes chocolate brownie. Chocolate muffin marzipan macaroon marshmallow candy canes I love fruitcake. I love carrot cake ice cream chupa chups jelly oat cake caramels donut. Gummi bears carrot cake I love carrot cake dragée muffin wafer.</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>


{{--        <div class="flex-center position-ref full-height">--}}
{{--            @if (Route::has('login'))--}}
{{--                <div class="top-right links">--}}
{{--                    @auth--}}
{{--                        <a href="{{ url('/home') }}">Home</a>--}}
{{--                    @else--}}
{{--                        <a href="{{ route('login') }}">Login</a>--}}

{{--                        @if (Route::has('register'))--}}
{{--                            <a href="{{ route('register') }}">Register</a>--}}
{{--                        @endif--}}
{{--                    @endauth--}}
{{--                </div>--}}
{{--            @endif--}}

{{--        </div>--}}
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>--}}
<script src="{{ asset('js/web.js') }}"></script>

</body>
</html>
