<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/187c0ed624.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/web.css') }}">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-default fixed-top">
            <a class="navbar-brand" href="#">{{ config('app.name', 'Laravel') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    {{--                <li class="nav-item dropdown">--}}
                    {{--                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--                        Dropdown--}}
                    {{--                    </a>--}}
                    {{--                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                    {{--                        <a class="dropdown-item" href="#">Action</a>--}}
                    {{--                        <a class="dropdown-item" href="#">Another action</a>--}}
                    {{--                        <div class="dropdown-divider"></div>--}}
                    {{--                        <a class="dropdown-item" href="#">Something else here</a>--}}
                    {{--                    </div>--}}
                    {{--                </li>--}}
                    {{--                <li class="nav-item">--}}
                    {{--                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>--}}
                    {{--                </li>--}}
                </ul>
                <div class="form-inline my-2 my-lg-0">
                    <div class="btn-group web-sidenav" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-primary"><i class="fas fa-chevron-down"></i></button>
                        <button type="button" class="btn btn-primary"><i class="far fa-bell"></i></button>
                        <div class="btn-group dropleft" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa-user-circle"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                @if (Route::has('login'))
                                    @auth
                                        <span class="dropdown-item"> {{ Auth::user()->name }}</span>
                                        <a class="dropdown-item" href="{{ route('home') }}"><i class="fas fa-sign-in-alt"></i> Continue</a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fas fa-sign-out-alt"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @else
                                        <form class="px-4 py-3" method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col-12">
                                                    <label for="email">Email address</label>
                                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{ old('email') }}" placeholder="email@example.com" required autocomplete="email">
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-12 mt-3">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" required autocomplete="current-password">
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-12 col-sm-6 mt-3">
                                                    <button type="submit" class="btn btn-primary">Sign in</button>
                                                </div>
                                                <div class="form-check col-12 col-sm-6 mt-3">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                        <label class="custom-control-label" for="remember">Remember me</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('register') }}">New around here? Sign up</a>
                                        @if (Route::has('password.request'))
                                            <a class="dropdown-item" href="{{ route('password.request') }}">Forgot password?</a>
                                        @endif
                                    @endauth
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <main class="global" style="background-image: url('{{ asset('img/1547-4.jpg') }}')">
            @yield('content')
        </main>
    </div>
    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
    @yield('javascript');


</body>
</html>
